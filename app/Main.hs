{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

import Application ()
import BuildInfo
import Foundation
import GHC.IO.Encoding (setLocaleEncoding, utf8)
import Network.Wai.Handler.Warp
import Utils
import Yesod.Core

main :: IO ()
main = do
  setLocaleEncoding utf8
  tPutStrLn [qq|⊰ download-server {versionText} ⊱ written by enefi|]
  flushStds
  app <- toWaiApp App
  let settings = defaultSettings & setHost "127.0.0.1" & setPort 4527 & setServerName ""
  runSettings settings app
