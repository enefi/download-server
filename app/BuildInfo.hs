module BuildInfo where

import Data.Version (showVersion)
import Paths_download_server
import Utils

versionText :: Text
versionText = showVersion version & toS
