# Download Server

It accepts requests in JSON and downloads a requested file (focus is on pictures).

Project status: beta - basic functionality is implemented, but it is not very customizable; API should change only minimally if at all

# Binaries (download)

Please see [Release](https://gitlab.com/enefi/download-server/-/releases) at the top.

# Building from source

## Requirements

* [stack](http://haskellstack.org)

## Installation

Clone (or download) this repository and navigate inside its directory.

Install it:

```sh
$ stack install
```

## Usage

```
$ download-server
```

## Configuring systemd service

Create a unit file `download-server.service` in systemd configuration directory (e.g. `/etc/systemd/system` on Manjaro). Replace `xxx` with your user and `/home/xxx/Downloads` with a directory you want to download files to.

```ini
[Unit]
After=network.target remote-fs.target

[Service]
ExecStart=/home/xxx/.local/bin/download-server
WorkingDirectory=/home/xxx/Downloads
Restart=always
User=xxx
StandardOutput=journal

[Install]
WantedBy=multi-user.target
```
Reload unit files and set it up to start automatically:
```sh
$ systemctl daemon-reload
$ systemctl enable download-server
```
Restart PC or start the service manually: `systemctl start download-server`

## API

### POST `/download`

Notes: `http` command is [httpie](https://httpie.org/), `exa` is an alternative to `ls`

```sh
$ http -v POST localhost:4527/download url=https://i.ibb.co/y4751nz/clown-pepe-nice.gif
POST /download HTTP/1.1
Accept: application/json, */*;q=0.5
Accept-Encoding: gzip, deflate
Connection: keep-alive
Content-Length: 55
Content-Type: application/json
Host: localhost:4527
User-Agent: HTTPie/2.2.0

{
    "url": "https://i.ibb.co/y4751nz/clown-pepe-nice.gif"
}

HTTP/1.1 200 OK
Content-Length: 31
Content-Type: text/plain; charset=utf-8
Date: Sat, 10 Oct 2020 18:53:30 GMT
Set-Cookie: _SESSION=xxx; Expires=Sat, 10-Oct-2020 20:53:29 GMT; HttpOnly
Vary: Accept, Accept-Language
X-XSS-Protection: 1; mode=block

Saved as "clown-pepe-nice.gif".

$ exa -l clown-pepe-nice.gif
.rw-r--r-- 750k xxx 10 Oct 20:53 clown-pepe-nice.gif
```

Requested file will be downloaded to a directory from which the server was started (current working directory).

## License

AGPL3 (GNU Affero General Public License v3) [full](https://www.gnu.org/licenses/agpl-3.0.en.html) [tldr](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0))
