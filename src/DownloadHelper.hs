{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module DownloadHelper where

-- This module uses lens package (instead of optics) because of wreq. Other libraries like req were either over-engineered,
-- e.g. many lines to construct a simple URL from Text, or lacking wanted features (HTTPS).

import Control.Applicative ((<|>))
import Control.Exception (SomeException, catch)
import Control.Lens hiding (re)
import Control.Monad (join, when)
import Control.Monad.Extra (ifM)
import Data.Aeson (toJSON)
import Data.Aeson.Lens (key, nth)
import Data.ByteString.Lazy (ByteString)
import Data.Maybe (fromMaybe)
import Data.String.Conv (toS)
import Data.Text (Text)
import Network.Wreq
import System.Directory.Extra (doesFileExist)
import Text.RE.PCRE.Text
import Text.RE.Tools

downloadFromUrlToMemory :: Text -> IO (Either Text (ByteString, Text))
downloadFromUrlToMemory url = do
  let opts = set checkResponse (Just $ \_ _ -> return ()) defaults
  r <- getWith opts (toS url)
  if r ^. responseStatus . statusCode == 200
    then return $ Right (r ^. responseBody, toS $ r ^. responseHeader "Content-Type")
    else return $ Left $ toS $ show (r ^. responseStatus . statusCode) <> ": " <> toS (r ^. responseStatus . statusMessage)

extensionFromContentType :: Text -> Maybe Text
extensionFromContentType x =
  lookup
    x
    [ ("image/jpeg", "jpg"),
      ("image/jpg", "jpg"),
      ("image/png", "png"),
      ("image/gif", "gif"),
      ("image/svg+xml", "svg"),
      ("image/webp", "webp"),
      ("image/tiff", "tiff")
    ]

sanitizeFileName :: Text -> Text
sanitizeFileName = (*=~/ [ed|[^a-zA-Z0-9-]///_|])

generateFileName :: Text -> Text -> IO (Maybe FilePath)
generateFileName url contentType = do
  fnFromUrlReplaceWithExtensionFromContentType <- (\x y -> x <> "." <> y) <$> (url & sanitizeFileName & toS & Just) <*> (extensionFromContentType contentType <&> toS) & validateFileNameMay
  fnFromUrl <- catch (fileNameFromUrl url) (\(e :: SomeException) -> return Nothing)
  fnFromSimpleUrlReplace <- sanitizeFileName url & toS & Just & validateFileNameMay
  -- TODO: improve file name generators to return list
  -- TODO: rewrite fileNameFromUrl to construct infinite list with increasing numbers when match succeeds
  return $ fnFromUrl <|> fnFromUrlReplaceWithExtensionFromContentType <|> fnFromSimpleUrlReplace
  where
    isFileNameValid :: FilePath -> IO Bool
    isFileNameValid x = do
      exists <- doesFileExist x
      return $ not exists
    validateFileNameMay :: Maybe FilePath -> IO (Maybe FilePath)
    validateFileNameMay Nothing = return Nothing
    validateFileNameMay (Just fn) = ifM (isFileNameValid $ toS fn) (return $ Just fn) (return Nothing)
    fileNameFromUrl :: Text -> IO (Maybe FilePath)
    fileNameFromUrl x = do
      -- TODO: get rid of regex package which throws on failed matching. try pcre-heavy or roll out big guns - megaparsec?
      let urlRegex = [re|^https?:\/\/.+\/([\w-]+)(?:\.(\w+))?(?:[\?|#].*)?$|]
      let urlMatch = url ?=~ urlRegex
      print urlMatch
      let partsMay =
            (,) <$> (urlMatch !$$? [cp|1|]) <*> (urlMatch !$$? [cp|2|])
              <&> over each sanitizeFileName
      case partsMay of
        Nothing -> return Nothing
        Just parts -> do
          let fnFromUrl = parts & (\(a, b) -> a <> "." <> b)
          validateFileNameMay $ Just $ toS fnFromUrl
