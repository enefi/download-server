{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}

module Utils
  ( tshow,
    tPutStr,
    tPutStrLn,
    tPutStrLnLn,
    tErrPutStrLn,
    tReadFile,
    tWriteFile,
    flushStdOut,
    flushStdErr,
    flushStds,
    (>>>),
    (&),
    (<&>),
    ($>),
    (<$),
    Text,
    bool,
    rightOrCrash,
    skip,
    toS,
    forM,
    forM_,
    void,
    qq,
    nl,
    pShow,
    firstErrorFromEithers,
    stripSuffixIfPresent,
    capitalize,
    deCapitalize,
    liftToText,
    indent,
  )
where

import Control.Arrow ((>>>))
import Control.Monad (forM, forM_, void)
import Data.Bool (bool)
-- import qualified Data.ByteString.Lazy as BL
-- import qualified Data.ByteString.Lazy.Char8 as CL
import Data.Function ((&))
import Data.Functor (($>), (<&>))
-- import qualified Data.Text.Lazy as TL
-- import qualified Data.Text.Lazy.Encoding as TLE
import Data.Maybe (fromMaybe)
import Data.String.Conv (toS)
import Data.Text (Text)
import qualified Data.Text as T
import System.IO (hFlush, hPutStrLn, stderr, stdout)
import System.IO.Extra (IOMode (..), hSetEncoding, utf8_bom, withFile)
import System.IO.Strict (hGetContents)
import Text.InterpolatedString.Perl6 (qq)
import qualified Text.Pretty.Simple as PrettySimple
import UtilsTH (capitalizeWordStr, deCapitalizeWordStr)

tshow :: Show a => a -> Text
tshow = show >>> T.pack

tPutStr :: Text -> IO ()
tPutStr = T.unpack >>> putStr

tPutStrLn :: Text -> IO ()
tPutStrLn = T.unpack >>> putStrLn

tPutStrLnLn :: Text -> IO ()
tPutStrLnLn x = x <> "\n" & tPutStrLn

tErrPutStrLn :: Text -> IO ()
tErrPutStrLn = toS >>> hPutStrLn stderr

flushStdOut :: IO ()
flushStdOut = hFlush stdout

flushStdErr :: IO ()
flushStdErr = hFlush stderr

flushStds :: IO ()
flushStds = flushStdOut >> flushStdErr

-- | Strict text file read in UTF8 encoding.
tReadFile :: Text -> IO Text
tReadFile fileName = do
  withFile (toS fileName) ReadMode $
    \h -> do
      hSetEncoding h utf8_bom
      contents <- System.IO.Strict.hGetContents h
      return $ toS contents

-- | Lazy text file write.
tWriteFile :: Text -> Text -> IO ()
tWriteFile fileName contents = writeFile (toS fileName) (toS contents)

rightOrCrash :: Show e => Either e a -> a
rightOrCrash (Right x) = x
rightOrCrash (Left e) = error $ show e

skip :: Monad m => m ()
skip = return ()

nl :: Text
nl = "\n"

pShow :: Show a => a -> Text
pShow = PrettySimple.pShowOpt opts >>> toS
  where
    opts = PrettySimple.defaultOutputOptionsDarkBg {PrettySimple.outputOptionsIndentAmount = 2}

firstErrorFromEithers :: [Either l r] -> Either l [r]
firstErrorFromEithers [] = Right []
firstErrorFromEithers ((Left e) : _) = Left e
firstErrorFromEithers ((Right r) : xs) = firstErrorFromEithers xs <&> (r :)

stripSuffixIfPresent :: Text -> Text -> Text
stripSuffixIfPresent s x = x & T.stripSuffix s & fromMaybe x

capitalize :: Text -> Text
capitalize = toS >>> capitalizeWordStr >>> toS

deCapitalize :: Text -> Text
deCapitalize = toS >>> deCapitalizeWordStr >>> toS

liftToText :: (String -> String) -> (Text -> Text)
liftToText f = toS >>> f >>> toS

indent :: Int -> Text -> Text
indent n x = x & T.lines <&> (T.replicate n " " <>) & T.unlines
