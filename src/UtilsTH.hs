module UtilsTH where

import Control.Arrow ((>>>))
import Data.Function ((&))
import Data.Aeson.TH
  ( SumEncoding (..),
    constructorTagModifier,
    defaultOptions,
    fieldLabelModifier,
    sumEncoding,
  )
import Data.Aeson.Types (Options)
import Data.Char (toLower, toUpper)

deCapitalizeWordStr :: String -> String
deCapitalizeWordStr "" = ""
deCapitalizeWordStr x = (x & head & toLower) : (x & tail)

capitalizeWordStr :: String -> String
capitalizeWordStr "" = ""
capitalizeWordStr x = (x & head & toUpper) : (x & tail)

derJsonOpts :: Options
derJsonOpts =
  defaultOptions
    { constructorTagModifier = deCapitalizeWordStr,
      sumEncoding = UntaggedValue
    }
