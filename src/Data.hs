{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Data where

import Data.Aeson.TH (defaultOptions, deriveJSON)
import Data.Text (Text)
import GHC.Generics (Generic)
import Optics (makeFieldLabels, makeFieldLabelsWith, noPrefixFieldLabels)
import UtilsTH

data DownloadOptions = DownloadOptions {url :: Text} deriving (Eq, Show, Generic)

deriveJSON derJsonOpts ''DownloadOptions

makeFieldLabelsWith noPrefixFieldLabels ''DownloadOptions
