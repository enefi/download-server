{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Download where

import Control.Monad (unless, when)
import Data
import Data.ByteString.Lazy (ByteString)
import qualified Data.ByteString.Lazy as B
import qualified Data.Text as T
import DownloadHelper
import Foundation
import Network.HTTP.Types.Status
import qualified Network.Wai as W
import Optics
import Utils
import Yesod.Core

-- probably no longer needed
onlyAllowedFromLocalhost :: Handler ()
onlyAllowedFromLocalhost = do
  let allowedIp = "127.0.0.1"
  host <- waiRequest <&> W.remoteHost <&> tshow
  unless (T.isPrefixOf (allowedIp <> ":") host) $
    sendResponseStatus forbidden403 ("Access is allowed only from " <> allowedIp)

postDownloadR :: Handler ()
postDownloadR = do
  onlyAllowedFromLocalhost
  task <- requireCheckJsonBody :: Handler DownloadOptions
  liftIO $ tPutStrLn $ "Processing task " <> pShow task
  let targetUrl = task ^. #url
  liftIO $ tPutStrLn $ "downloading " <> pShow targetUrl
  dataEither <- liftIO $ downloadFromUrlToMemory targetUrl
  case dataEither of
    Left err -> sendResponseStatus status500 ("Download failed with: " <> err)
    Right (fileData, contentType) -> handleImageInMemory targetUrl fileData contentType
  where
    handleImageInMemory targetUrl fileData contentType = do
      liftIO $ tPutStrLn $ "generating filename " <> pShow (targetUrl, contentType)
      fnMay <- liftIO $ generateFileName targetUrl contentType
      case fnMay of
        Nothing -> sendResponseStatus status500 ("Failed to generate a file name" :: Text)
        Just fn -> handleFileSaving fileData fn
    handleFileSaving :: ByteString -> FilePath -> Handler ()
    handleFileSaving fileData fileName = do
      liftIO $ tPutStrLn $ "saving file " <> pShow fileName
      liftIO $ B.writeFile fileName fileData
      sendResponseStatus ok200 ([qq|Saved as "$fileName".|] :: Text)
